from socket import *
import base64
import time

mail_server = ("mail.gmx.com", 25)


def authenticate(sock, message, username, password):
    heloCommand = 'EHLO ' + message + '\r\n'
    sock.send(heloCommand.encode())
    response = sock.recv(1024)
    response = response.decode()

    # print("Message after EHLO command:" + response)
    # if response[:3] != '250':
    #     print('250 reply not received from server.')

    # Info for username and password
    # username = "arefmrd@gmx.com"
    # password = "samplepassword"

    encoded = ("\x00" + username + "\x00" + password).encode()
    encoded = base64.b64encode(encoded)

    authentication_message = "AUTH PLAIN ".encode() + encoded + "\r\n".encode()
    sock.send(authentication_message)
    response_auth = sock.recv(1024)

    print('Authentication result ' + response_auth.decode())


def sendMAILFROMcommand(sock, sender):
    mailFrom = "MAIL FROM:<" + sender + ">\r\n"
    sock.send(mailFrom.encode())
    response = clientSocket.recv(1024)
    response = response.decode()
    print("MAIL FROM command result: " + response)


def sendRCPTcommand(sock, recipient):
    rcptTo = "RCPT TO:<" + recipient + ">\r\n"
    sock.send(rcptTo.encode())
    response = sock.recv(1024)
    response = response.decode()
    print("RCPT TO command result: " + response)


def sendDATAcommand(sock, message, subject_text):
    data = "DATA\r\n"
    end_msg = "\r\n.\r\n"
    sock.send(data.encode())
    data_response = sock.recv(1024)
    data_response = data_response.decode()
    print("DATA command result: " + data_response)

    subject = "Subject: " + subject_text + "\r\n\r\n"
    sock.send(subject.encode())
    date = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
    date += "\r\n\r\n"
    sock.send(date.encode())
    sock.send(message.encode())
    sock.send(end_msg.encode())
    message_response = sock.recv(1024)
    print("Response after sending message body: " + message_response.decode())


def sendQUITcommand(sock):
    quit_cmd = "QUIT\r\n"
    sock.send(quit_cmd.encode())
    quit_response = sock.recv(1024)
    print(quit_response.decode())


clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect(mail_server)
socket_response = clientSocket.recv(1024)
socket_response = socket_response.decode()

print("Connection request:" + socket_response)

if socket_response[:3] != '220':
    print('220 reply not received from server.')

authenticate(clientSocket, "AREF", "arefmrd@gmx.com", "samplepassword")

sendMAILFROMcommand(clientSocket, "arefmrd@gmx.com")

sendRCPTcommand(clientSocket, "aref_mrd@yahoo.com")

sendDATAcommand(clientSocket, "Hey there how u doin?", "Howdy partner")

sendQUITcommand(clientSocket)

clientSocket.close()
